#12/1/2019
# GradNormal Algorithm Python program
# compute  normal surface generated B_  Goldberg tiling

#This python program is meant only as a proof of concept
#There is much room to improve running time and efficiency
#It is not set up to input general surfaces or for general usage

#The output of this program is three .off files
#surfacePts.off   - output of MidNormal algorithm for given a
#gsurfacePts.off - projection of output of MidNormal algorithm using gradient
#noquadSurfacePts.off - adjusts gsurfacePts.off as in GradNormal algorithm by
# replacing 4 triangles neighboring valence four vertices with 2 triangles.


import math
import numpy as np


#PICK A CASE by uncommenting
case = "sphere"
#case = "genus2"
#case = "genus5"
 
N = 25   #Will create a tiling of the unit cube by NxNxNx6 tetrahedra 
#Up to N=100 is runnable within an hour.

# Set Parameters  - a and N

a = math.sqrt(2.)/4.  #  approx 0.35355 from paper - use for GradNorm1
#a = math.sqrt(3.)/4.  #  approx  0.43 from paper - use for MidNorm
#a = 0.43
#a= 0.2349   # select Goldberg constant 0.2349
#a=0.314  # Good for minimizing maximum angle 

#a= 0.4798  #really  math.sqrt(3*math.sqrt(17)/32  - 5/32)#   about 0.4798 
#a= 0.30  # select Goldberg constant  
#a= 0.45  # sample Goldberg constant  
#a= 0.4798   # sample Goldberg constant 0.4798
#a= 0.40 # sample Goldberg constant  
 
cutoff = .000000001 
#merge vertices tolerance - to stop numerical errors from 
#invalidating test for equality of points in later computations.

print  ("N= ", N)
print ("a value",  a)
print ("Using quads KLP and KPQ ")

##################################################################################
# define function and gradient used for 0-level set extraction

if(case == "sphere"):
    def myf(x, y, z):  #x,y,z in [0,1]^3
        return (x - 0.5)**2 +(y - 0.5)**2 + (z - 0.5)**2 - 0.1 
    def mygradf(x, y, z):  #x,y,z in [0,1]^3
        return [2*(x - 0.5) , 2*(y - 0.5) , 2*(z - 0.5)]  
    print("running midNormal for sphere") 
##################################################################################

if(case == "genus2"):
    def myf(x, y, z):  #x,y,z in [0,1]^3
        x = (3*x-1.5)
        y = (3*y-1.5)
        z = (3*z-1.5)
        return(  ((x**2 + y**2)**2 -x**2  + y**2)**2 +z**2 - 0.028 )
    def mygradf(x, y, z):  #x,y,z in [0,1]^3
        x = (3*x-1.5)
        y = (3*y-1.5)
        z = (3*z-1.5)
        return([2*((x**2 + y**2)**2 -x**2 + y**2)*(2*(x**2 + y**2)*2*x -2*x)*3 , 2*((x**2 + y**2)**2 -x**2 + y**2)*(2*(x**2 + y**2)*2*y +2*y)*3 ,2*z*3])
    print("running midNormal for genus2") 

##################################################################################

if(case == "genus5"):
	def myf(x, y, z):  #x,y,z in [0,1]^3
		x = (3*x-1.5)
		y = (3*y-1.5)
		z = (3*z-1.5)
		return( x**4 + y**4 + z**4  - (x**2  + y**2 + z**2) + 0.4 ) 
	def mygradf(x, y, z):  #x,y,z in [0,1]^3
		x = (3*x-1.5)
		y = (3*y-1.5)
		z = (3*z-1.5)
		return [ (4*x**3 -2*x)*3, (4*y**3 -2*y)*3 ,(4*z**3 -2*z)*3]
	print("running midNormal for genus5") 


##################################################################################

class Point:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z
        self.cutoff = 0.00000001

    def to_string(self):
        return "%s %s %s" % (self.x, self.y, self.z)

    #define  function for zero level set given by sphere centered at (0.5,0.5,0.5)
    def level_f(self):
    	return (myf(self.x, self.y, self.z) )
 
    #define gradient of function giving sphere centered at (0.5,0.5,0.5)
    def gradf_x(self):
        return ( mygradf(self.x, self.y, self.z)[0])

    def gradf_y(self):
        return (mygradf(self.x, self.y, self.z)[1])

    def gradf_z(self):
         return (mygradf(self.x, self.y, self.z)[2])

    #define norm squared of gradient of function for sphere centered at (0.5,0.5,0.5)
    def normsqrdGradf(self):
        val =  self.gradf_x()**2 + self.gradf_y()**2 + self.gradf_z()**2
        return val

    #define vector that repositions vertices
    def displace_x(self):
        normsG = self.normsqrdGradf()
        if  -self.cutoff < normsG < self.cutoff:   #  set cutoff for near level set
             val = 0
        else:
             val = - self.level_f()*self.gradf_x()/self.normsqrdGradf()
        return val

    def displace_y(self):
         normsG = self.normsqrdGradf()
         if  -self.cutoff < normsG < self.cutoff:
             val = 0
         else:
             val =  -  self.level_f()*self.gradf_y()/self.normsqrdGradf()
         return val

    def displace_z(self):
          normsG = self.normsqrdGradf()
          if  -self.cutoff < normsG < self.cutoff:
             val = 0
          else:
             val = - self.level_f()*self.gradf_z()/self.normsqrdGradf()
          return val

    def displaced_point(self):
        displaced = Point(self.x,self.y,self.z)
        displaced.x = self.x + self.displace_x()
        displaced.y = self.y + self.displace_y()
        displaced.z = self.z + self.displace_z()
        return displaced

    @staticmethod
    def midpoint(p1,p2):
        return Point((p1.x + p2.x)/2., (p1.y + p2.y)/2., (p1.z + p2.z)/2.)

class Triangle:
    def __init__(self, p1, p2, p3):
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3

    def displaced_triangle(self):
        return Triangle(
            self.p1.displaced_point(), 
            self.p2.displaced_point(), 
            self.p3.displaced_point()
        )

    def to_string(self):
        return "%s\n%s\n%s\n" % (self.p1.to_string(), self.p2.to_string(), self.p3.to_string())

    def minMaxAngles(self):
        vec1=[self.p2.x-self.p1.x,self.p2.y-self.p1.y,self.p2.z-self.p1.z]
        vec2=[self.p3.x-self.p2.x,self.p3.y-self.p2.y,self.p3.z-self.p2.z]
        vec3=[self.p1.x-self.p3.x,self.p1.y-self.p3.y,self.p1.z-self.p3.z]
        angle1=np.arccos((-1 )*np.dot(vec1,vec3)/(np.linalg.norm(vec1)*np.linalg.norm(vec3)))
        angle2=np.arccos((-1)*np.dot(vec1,vec2)/(np.linalg.norm(vec1)*np.linalg.norm(vec2)))
        angle3=np.arccos((-1)*np.dot(vec2,vec3)/(np.linalg.norm(vec2)*np.linalg.norm(vec3)))
        smallest = min(angle1, angle2, angle3)
        largest = max(angle1, angle2, angle3)
        return smallest, largest

Pi = math.pi #
sqrt3 = math.sqrt(3.)  #square root of 3

Nx=N+1   #  x-direction triangle count.  1 extra needed
Ny= int(math.ceil(N*2/sqrt3)) #   y-direction triangle count
Nz=  int(math.ceil( float(N)/(3. * a) +1))  # z-direction count of tetrahedra stacked above a triangle
dx = 1./float(N)
dy = dx*sqrt3/2.
dz = 3.*a/float(N)


##################################################################################

#Function that gives three internal angles of a triangle
def angles(T):  # T is a triangle
    vec1=[T.p2.x-T.p1.x,T.p2.y-T.p1.y,T.p2.z-T.p1.z]
    vec2=[T.p3.x-T.p2.x,T.p3.y-T.p2.y,T.p3.z-T.p2.z]
    vec3=[T.p1.x-T.p3.x,T.p1.y-T.p3.y,T.p1.z-T.p3.z]
    angle1=np.arccos((-1 )*np.dot(vec1,vec3)/(np.linalg.norm(vec1)*np.linalg.norm(vec3)))
    angle2=np.arccos((-1)*np.dot(vec1,vec2)/(np.linalg.norm(vec1)*np.linalg.norm(vec2)))
    angle3=np.arccos((-1)*np.dot(vec2,vec3)/(np.linalg.norm(vec2)*np.linalg.norm(vec3)))
    return angle1, angle2, angle3

#initialize min and max angles (radians)
minAngle = Pi
maxAngle = 0
#initialize min and max angles (radians) after grad projection
gminAngle = Pi
gmaxAngle = 0.

# Writes the triangle, and the displaced triangle defined by points p1, p2, p3 to 
# the given files.  Updates min and max angles
def write_triangle(p1, p2, p3, triangle_file, gradient_triangle_file):
    global minAngle, maxAngle, gmaxAngle, gminAngle
    t = Triangle(p1, p2, p3)
    t_displaced = t.displaced_triangle()
    triangle_file.write(t.to_string())
    gradient_triangle_file.write(t_displaced.to_string())
    #update min and max angles
    a,b,c = angles(t)
    if (a > maxAngle):  maxAngle = a
    if (b > maxAngle):  maxAngle = b
    if (c > maxAngle):  maxAngle = c
    if (a < minAngle):  minAngle = a
    if (b < minAngle):  minAngle = b
    if (c < minAngle):  minAngle = c
    #update min and max angles after gradient adjustmenmt
    a,b,c = angles(t_displaced)
    if (a > gmaxAngle):  gmaxAngle = a
    if (b > gmaxAngle):  gmaxAngle = b
    if (c > gmaxAngle):  gmaxAngle = c
    if (a < gminAngle):  gminAngle = a
    if (b < gminAngle):  gminAngle = b
    if (c < gminAngle):  gminAngle = c

triangleCount = 0  # counts number of vertices appearing in triangle list.  
triangleCoords = open("triangleCoordFile.txt", 'w') # file to write triangle coords 
gtriangleCoords = open("gtriangleCoordFile.txt", 'w') # file to write normalized triangle coords 

for i in range(Nx):
    for j in range(Ny):  
        for k in range(Nz):   #x,y,z coordinates of i,j,k lattice point 
            A_x = (float(i) -(0.5)*(j % 2))*dx
            A_y = (float(j))*dy
            A_z = float(k)*dz + float(i % 3) * dz/3. -dz + float(j % 2)*dz/3.   # lowest z coordinate starts below xy plane.
            A = Point(A_x, A_y, A_z)
            B = Point(A.x, A.y, A.z + dz)
            #x,y,z coordinates for unchanged edge midpoints
            K = Point.midpoint(A,B)

            #Evaluate function levelf at vertices A,B. 
            fA = A.level_f()
            fB = B.level_f()
            #x,y,z coordinates for four vertices A,B,C,D of six tetrahedra whose lowest z coordinate vertex is at P
            #z coordinates have A < C < D < B           
            # TODO: Write tetrahedra class, should move this logic there.
            for l in range(6): 
                if l % 2: #  if l is odd
                    C_x = A.x + math.cos(float(l+1)*Pi/3.)*dx
                    C_y = A.y + math.sin(float(l+1)*Pi/3.)*dx              
                    D_x = A.x + math.cos(float(l)*Pi/3.)*dx
                    D_y = A.y + math.sin(float(l)*Pi/3.)*dx                            
                else:  #  if l  even 
                    C_x = A.x + math.cos(float(l)*Pi/3.)*dx     
                    C_y = A.y + math.sin(float(l)*Pi/3.)*dx  
                    D_x = A.x + math.cos(float(l+1)*Pi/3.)*dx   
                    D_y = A.y + math.sin(float(l+1)*Pi/3.)*dx 
                C_z = A.z + dz/3.
                D_z = A.z + 2*dz/3.

                C = Point(C_x, C_y, C_z) 
                D = Point(D_x, D_y, D_z)
                # Now evaluate function levelf at vertices.
                fC = C.level_f()
                fD = D.level_f()
                # Find x,y,z coordinates for edge midpoints
                L = Point.midpoint(A,C)
                M = Point.midpoint(A,D)
                N = Point.midpoint(B,C)
                P = Point.midpoint(C,D)
                Q = Point.midpoint(B,D)
                if (fA >= 0 and fB >= 0 and fC >= 0 and fD >= 0) or (fA < 0 and fB < 0 and fC < 0 and fD < 0):
                   triangleCount += 0		  # no triangles added for this one sign tetrahedron - treat 0 as positive
                elif (fA >= 0 and fB < 0 and fC < 0 and  fD < 0) or (fA < 0 and fB >= 0 and fC >= 0 and fD >= 0):
                    # Case of triangle near vertex A
                    write_triangle(K, L, M, triangleCoords, gtriangleCoords)
                    triangleCount += 1
                elif (fB >= 0 and fA < 0 and fC < 0 and fD < 0) or (fB < 0 and fA >= 0 and fC >= 0 and fD >= 0):
                    # Case of triangle near vertex B
                    write_triangle(K, N, Q, triangleCoords, gtriangleCoords)
                    triangleCount += 1	
                elif (fC >= 0 and fA < 0 and fB < 0 and fD < 0) or (fC < 0 and fB >= 0 and fA >= 0 and fD >= 0):
                   # Case of triangle near vertex C
                    write_triangle(P, N, L, triangleCoords, gtriangleCoords)
                    triangleCount += 1
                elif (fD >= 0 and fA < 0 and fB < 0 and fC < 0) or (fD < 0 and fB >= 0 and fA >= 0 and fC >= 0):
                   # Case of triangle near vertex D
                    write_triangle(P, M, Q, triangleCoords, gtriangleCoords)
                    triangleCount += 1
                elif (fA >= 0 and fB >= 0 and fC < 0 and fD < 0) or (fA < 0 and fB< 0 and fC >= 0 and fD >= 0): 
                   # Case of quadrilateral LMNQ separating AB, CD
                    write_triangle(L, M, N, triangleCoords, gtriangleCoords)
                    write_triangle(M, N, Q, triangleCoords, gtriangleCoords)
                    triangleCount += 2		
                elif (fA >= 0 and fC >= 0 and fB < 0 and fD < 0) or (fA < 0 and fC< 0 and fB >= 0 and fD >= 0):
                    # Case of quadrilateral KMPN separating AC, BD
                    write_triangle(K, M, N, triangleCoords, gtriangleCoords)
                    write_triangle(M, N, P, triangleCoords, gtriangleCoords)
                    triangleCount += 2       
                elif(fA >= 0 and fD >= 0 and fC < 0 and fB < 0) or (fA < 0 and fD< 0 and fC >= 0 and fB >= 0):  
                   # Case of quadrilateral KLPQ separating AD, CB used when a=0.47  Use KLP and KPQ for a=0.47. Use KLQ and LPQ for a=0.23.  Different quads subdivisions into triangles get used.
#                   if a == 0.4798:
                    if a >= math.sqrt(2)/4:  # Use KLP and KPQ
                        write_triangle(K, L, P, triangleCoords, gtriangleCoords)
                        write_triangle(K, P, Q, triangleCoords, gtriangleCoords)
                        triangleCount += 2
                    else: 
                    	a == 0.2349   #write triangle KLQ and LPQ to file
                        write_triangle(K, L, Q, triangleCoords, gtriangleCoords)
                        write_triangle(L, P, Q, triangleCoords, gtriangleCoords)
                        triangleCount += 2
#                     else: #  Use KLP and KPQ  for other a
#                         write_triangle(K, L, P, triangleCoords, gtriangleCoords)
#                         write_triangle(K, P, Q, triangleCoords, gtriangleCoords)
#                         triangleCount += 2
triangleCoords.close()
gtriangleCoords.close()

#Create surfacePts.off file.  For now, it has duplicate points .
#Create gsurfacePts.off file with projected points. For now, it has duplicate points.

#Create first two lines of .off file in surfacePts.off  and list  v,f,e
surface = open("surfacePts.off", 'w')
gsurface = open("gsurfacePts.off", 'w')
Mystring = "OFF \n"
Mystring += "%s %s %s\n" % (triangleCount*3, triangleCount, triangleCount*3)  #  v,f,e
surface.write(Mystring)
gsurface.write(Mystring)
triangleCoords = open("triangleCoordFile.txt", 'r') # file to write triangle coords
gtriangleCoords = open("gtriangleCoordFile.txt", 'r') # file to write triangle coords
fileContents2 =  triangleCoords.read()
gfileContents2 =  gtriangleCoords.read()
surface.write(fileContents2)
gsurface.write(gfileContents2)
#append triangle descriptions to end of file surfacePts.txt
for i in range(triangleCount):
    Mystring = "%s %s %s %s\n" % (3, 3*i, 3*i+1, 3*i+2)  # number of edges and  vertex indices
    surface.write(Mystring)
    gsurface.write(Mystring)
surface.close()
gsurface.close()
print("Initial surface has ", 3*triangleCount, "vertices and ",triangleCount , "faces")  

print ("Minimum normal surface angle: ",  minAngle*180/Pi, "degrees")
print("Maximum normal surface angle: ", maxAngle*180/Pi, "degrees")
print ("Minimum projected surface angle: ", gminAngle*180/Pi, "degrees")
print ("Maximum projected surface angle: ", gmaxAngle*180/Pi, "degrees")

######################### Retriangulate - eliminate valence four triangles
  
surfacefile = open("gsurfacePts.off", 'r')

if 'OFF' != surfacefile.readline().strip():
    raise('Not a valid OFF header')     
    
n_verts, n_faces, n_dontknow = tuple([int(s) for s in surfacefile.readline().strip().split(' ')])

verts = [[float(s) for s in surfacefile.readline().strip().split(' ')] for i_vert in range(n_verts)]
 
faces = [[int(s) for s in surfacefile.readline().strip().split(' ')][1:] for i_face in range(n_faces)]

print( "Initial surface has", n_verts, "vertices and",n_faces , "faces and Euler characteristic", n_verts- 3*n_faces +n_faces)  

#newVerts is a copy of verts.
newVerts  = [[0.,0.,0.] for i in range(len(verts))]
for i in range(0, len(verts)):
    newVerts[i] = verts[i]

# sort and eliminate duplicate vertices
newVerts.sort()
for i in range(n_verts-1, 0, -1): #count down from end, stop at i=1  n_verts-1?
    if(abs(newVerts[i][0]- newVerts[i-1][0]) < cutoff and 
    abs(newVerts[i][1]- newVerts[i-1][1]) < cutoff and abs(newVerts[i][2]- newVerts[i-1][2]) < cutoff):
        del newVerts[i]

newn_verts=len(newVerts) #number of non-duplicated vertices now stored in newVerts

print(n_verts - newn_verts, "duplicate vertices were eliminated.")  

#compute new indices assigned to old vertices to use in defining triangles    
newindex  = [0 for i in range(n_verts)]
for  i in range(0, n_verts):
    for  j in range(0, newn_verts):
        if(abs(verts[i][0] - newVerts[j][0])< cutoff and abs(verts[i][1] - newVerts[j][1])< cutoff and  abs(verts[i][2] - newVerts[j][2])< cutoff):
            newindex[i] = j

#Create new faces list with permuted indices to match permutation of verts
newFaces  = [[0,0,0] for i in range(n_faces)]
for i in range(n_faces):
    for j in range(3):
        newFaces[i][j] =  newindex[faces[i][j]]

#eliminate duplicate faces - should not be needed
#newFaces.sort()
#for i in range(n_faces-1, 0, -1): #count down from end, stop at i=1 ?
#    if(abs(newFaces[i][0]- newFaces[i-1][0]) < cutoff and 
#    abs(newFaces[i][1]- newFaces[i-1][1]) < cutoff and abs(newFaces[i][2]- newFaces[i-1][2]) < cutoff):
#        del newFaces[i]

v=len(newVerts)
f=len(newFaces)
e= 3*f/2
Euler = v-e+f
print( "Surface with duplicated vertices deleted has", v, "vertices and",f , "faces and Euler characteristic", Euler)  

#count valence of new vertices in array valence
valence  = [0 for i in range(v)] #list of [vertex index, vertex valence]
for i in range(f):
    for j in range(3):
         valence[newFaces[i][j]]   +=1     

val4Pts = []  #list of valence 4 points
quadindSet = set() #set valence 4 indices
for i in range(len(valence)):
    if valence[i] == 4:
        val4Pts.append(i)
        quadindSet |= {i}
        
num4Pts = len(val4Pts)

#who are my neighbors?
neighbors = [set() for i in range(0,v)]  #one set for each vertex
for i in range(f):
    for j in range(3):
        currentV = newFaces[i][j]  #vertex of ith triangle
        neighbors[currentV] |= set(newFaces[i])   #set union
for i in range(0, v):  #not my own neighbor
    neighbors[i].discard(i)   
   
#For a face that is a quad, identify its good diagonal, the one not between two valence 6 vertices
newTriang = [[0,0,0]]
countnTri=0
for i in range(len(val4Pts)):  # of vertices
	qv = list(neighbors[val4Pts[i]])  #work with list of four neighbors
	if qv[0] in neighbors[qv[1]] and qv[0] in neighbors[qv[2]]: #diag is 0,3 or 1,2
		if valence[qv[0]]  > 6 or  valence[qv[3]]  > 6:     # diag 1,2
			newTriang.append([qv[0], qv[1], qv[2]]) 
			countnTri +=1
			newTriang.append([qv[1], qv[2], qv[3]]) 				
			countnTri +=1
		else: #diag  0,3
			newTriang.append([qv[0], qv[1], qv[3]]) 
			countnTri +=1
			newTriang.append([qv[0], qv[2], qv[3]]) 
			countnTri +=1            
	elif qv[0] in neighbors[qv[1]] and qv[0] in neighbors[qv[3]]: #diags are 0,2 and 1,3
		if valence[qv[0]]  > 6 or  valence[qv[2]]  > 6 : #diag  1,3
			newTriang.append([qv[0], qv[1], qv[3]]) 
			countnTri +=1
			newTriang.append([qv[1], qv[2], qv[3]]) 
			countnTri +=1            
		else: # diag  0,2
			newTriang.append([qv[0], qv[2], qv[3]]) 
			countnTri +=1
			newTriang.append([qv[0], qv[1], qv[2]]) 
			countnTri +=1            
	else: #diags are 0,1 and 2,3
		if valence[qv[0]]  > 6 or  valence[qv[1]]  > 6: # diag 2,3
			newTriang.append([qv[1], qv[2], qv[3]]) 
			countnTri +=1
			newTriang.append([qv[0], qv[2], qv[3]]) 
			countnTri +=1            
		else: ## diag 0,1
			newTriang.append([qv[0], qv[1], qv[2]]) 
			countnTri +=1
			newTriang.append([qv[0], qv[1], qv[3]]) 
			countnTri +=1             
del newTriang[0] #delete unneeded initial triangle used to initialize

numnqTri = len(newFaces) - 4*num4Pts  #number of triangles not in a quad

# nqFaces will contain all triangle vertices after adding diagonals to quads
#vertex of valence > 6
nqFaces  = []  
count=0
for i in range(len(newFaces)):
    k = newFaces[i]        
    if (valence[k[0]]  != 4 ) and  (valence[k[1]] ) != 4   and (valence[k[2]] != 4):        #if triangle has no valence 4 vertex
        nqFaces.append(k) 
        count +=1
#now add new triangles to nqFaces
for i in range(len(newTriang)):  # = countnTri =  2*num4Pts 
        nqFaces.append(newTriang[i])

#NOTE - verts of valence 4 have not been deleted but are not used in newFaces

#Create new cleaned up .off file, cleanedSurfacePts.off
#First create first two lines of .off file   and list  v,f,e
v=len(newVerts)   
f=len(nqFaces)
e= int(3*f/2)
surface = open("noquadSurfacePts.off", 'w')
Mystring = "OFF \n"
Mystring += "%s %s %s\n" % (v, f, e)  #  v,f,e
#write vertices, including now unused 
for i in range(0, v):
    Mystring += "%s %s %s \n" % (newVerts[i][0], newVerts[i][1], newVerts[i][2]) 
#write faces
for i in range(0, f):
#    Mystring += "%s %s %s %s %s %s %s \n" % (3,  nqFaces[i][0] , nqFaces[i][1] ,  nqFaces[i][2], 0., 1.,0.) #green
    Mystring += "%s %s %s %s  \n" % (3,  nqFaces[i][0] , nqFaces[i][1] ,  nqFaces[i][2] ) 
surface.write(Mystring)
surface.close()
 

################## get max and min angles, lengths
  
 
from numpy.linalg import norm

surfacefile = open("noquadSurfacePts.off", 'r')
 
if 'OFF' != surfacefile.readline().strip():
	raise('Not a valid OFF header') 	
	
n_verts, n_faces, n_dontknow = tuple([int(s) for s in surfacefile.readline().strip().split(' ')])

verts = [[float(s) for s in surfacefile.readline().strip().split(' ')] for i_vert in range(n_verts)]

faces = [[int(s) for s in surfacefile.readline().strip().split(' ')][1:] for i_face in range(n_faces)]

def getAngles(x1,y1,z1,x2,y2,z2,x3,y3,z3):
    vec1=[x2-x1,y2-y1,z2-z1]
    vec2=[x3-x2,y3-y2,z3-z2]
    vec3=[x1-x3,y1-y3,z1-z3]
    a = np.array(vec1) 
    b = np.array(vec2) 
    c = np.array(vec3) 
    if norm(np.cross(a,b)) > cutoff:  #	triangle not degenerate
        angle1=np.arccos((-1)*np.dot(vec1,vec3)/(np.linalg.norm(vec1)*np.linalg.norm(vec3)))
        angle2=np.arccos((-1)*np.dot(vec1,vec2)/(np.linalg.norm(vec1)*np.linalg.norm(vec2)))
        angle3=np.arccos((-1)*np.dot(vec2,vec3)/(np.linalg.norm(vec2)*np.linalg.norm(vec3)))
        return angle1, angle2, angle3
    else: return(Pi/3,Pi/3,Pi/3)  #bad triangle - return 60 degrees - doesn't affect max or min

def getLengths(x1,y1,z1,x2,y2,z2,x3,y3,z3):
    vec1=[x2-x1,y2-y1,z2-z1]
    vec2=[x3-x2,y3-y2,z3-z2]
    vec3=[x1-x3,y1-y3,z1-z3]
    a = np.array(vec1) 
    b = np.array(vec2) 
    c = np.array(vec3) 
    len1 =norm(a) 
    len2 =norm(b) 
    len3 =norm(c) 
    return len1,len2,len3


#Compute angles
v= len(verts)
f=len(faces)
minAngleindex = -1
maxAngleindex = -1
smallestAngle = Pi
biggestAngle = 0.
smallestLen = 1
biggestLen = 0.
poorTriangle = [-1  for i in range(f)]
angleHistogram = [0 for i in range(180)]  # angles between 0 and 180 degrees
currentAngle = [0,0,0]
for i in range(0, f):
    x1,y1,z1 = verts[faces[i][0]][0], verts[faces[i][0]][1], verts[faces[i][0]][2]
    x2,y2,z2 = verts[faces[i][1]][0], verts[faces[i][1]][1], verts[faces[i][1]][2]
    x3,y3,z3 = verts[faces[i][2]][0], verts[faces[i][2]][1], verts[faces[i][2]][2]
    currentAngle = getAngles(x1,y1,z1,x2,y2,z2,x3,y3,z3)
    currentLen = getLengths(x1,y1,z1,x2,y2,z2,x3,y3,z3)
    for j in range(3):
        if(currentAngle[j] < smallestAngle):
            smallestAngle = currentAngle[j]
            minAngleindex = i
        if(currentAngle[j]  < .1):
            poorTriangle[i] = 1
        if(currentAngle[j]  > biggestAngle):
            biggestAngle = currentAngle[j]
            maxAngleindex = i
        if(currentAngle[j]  > 3.):
            poorTriangle[i] = 1
        if(currentLen[j] < smallestLen):
            smallestLen = currentLen[j]
        if(currentLen[j]  > biggestLen):
            biggestLen = currentLen[j]
        #Keep track of all angles
        currentAngleDegrees = currentAngle[j] *180/Pi  #Between 0 and 180
        bin = int(np.floor(currentAngleDegrees))
        angleHistogram[bin] +=1   # add 1 to angles in that bin

print("There are" , num4Pts , "points of valence 4. \n  After valence-4 removal, vertex count = ", v , " and triangle count = ", f)
print("The Euler characteristic, assuming surface is a manifold, is now  ", v -num4Pts -f/2  )

print("smallest angle after valence-4 removal = ", smallestAngle*180/Pi,"degrees")
print("biggest angle after valence-4 removal = ", biggestAngle*180/Pi, "degrees")
#print("smallest angle occurs for index ", minAngleindex)
#print("biggest angle occurs for index ", maxAngleindex)

print("smallest edge length = ", smallestLen)
print("biggest edge Length = ", biggestLen)
print("biggest length ratio is ", biggestLen/smallestLen)
  